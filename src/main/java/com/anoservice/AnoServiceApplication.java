package com.anoservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnoServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnoServiceApplication.class, args);
	}

}
